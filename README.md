# "Digitale Basiskompetenzen im Lehramt"

Im Rahmen der Werkstatt des #OERcamp am LISUM im November 2019 hat die Community "Digitale Basiskompetenzen im Lehramt" begonnen, offene Materialien für den Einsatz im Lehramtsstudium zu erstellen. 

## Zielsetzung

"*Und Lehrkräfte müssen sich auskennen, wie digitale Medien - Hard- und Software - für ihr spezifisches Fach sinnvoll genutzt werden kann*." (Eickelmann, im Zeit online Interview, 6.11.2019)

Dieses Projekt soll Lehrende in diesem Sinn unterstützen. Hierzu werden Materialien bereitgestellt, die sich auf grundlegende Kompetenzen im Umgang und beim Einsatz digitaler Medien beziehen.

Die Materialien sind als Hilfestellung, aber nicht als fertig geplante Lehr-Lerneinheiten zu verstehen. Es bedarf immer einer didaktischen Anpassungen.

## Aufbau

Alle Materialien sollen Angaben/Hinweise enthalten zu 
* Kompetenzen (Bezug: DigCompEdu)
* Format
* Lernziele
* Aufgaben
* Zielgruppe


